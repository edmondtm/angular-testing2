import { Injectable } from '@angular/core';
import { DemoService } from './demo.service';
import { getQueryValue } from '@angular/core/src/view/query';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private demoService: DemoService) {
  }

  getValue(): string {
    return this.demoService.getValue();
  }

}
