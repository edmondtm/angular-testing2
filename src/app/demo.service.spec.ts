import { TestBed } from '@angular/core/testing';

import { DemoService } from './demo.service';
import { doesNotThrow } from 'assert';

describe('DemoService', () => {
  let service: DemoService;
  beforeEach(() => service = new DemoService());

  it('#getValue should return real value', () => {
    expect(service.getValue()).toBe('real value');
  });

  it('#getObservable should return real value from observable',
    (done: DoneFn) => {
      service.getObservable().subscribe(value => {
        expect(value).toBe('observable value');
        done();
      });
    }
  );

  it('#getPromise should return real value for promise',
    (done: DoneFn) => {
      service.getPromise().then(value => {
        expect(value).toBe('promise value');
        done();
      });
    });

});
