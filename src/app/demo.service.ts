import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor() { }

  getValue(): string {
    return 'real value';
  }

  getObservable(): Observable<string> {
    let obs: Observable<string>;
    obs = of('observable value');
    return obs;
  }

  getPromise(): Promise<string> {
    const promise: Promise<string> = new Promise((resolve, reject) => {
      // the resolve / reject functions control the fate of the promise
      resolve('promise value');
      reject('reject123');
    });
    return promise;
  }

}
