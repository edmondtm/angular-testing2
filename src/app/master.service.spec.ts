import { TestBed } from '@angular/core/testing';

import { MasterService } from './master.service';
import { DemoService } from './demo.service';


let masterService: MasterService;
let demoServiceSpy: jasmine.SpyObj<DemoService>;



describe('MasterService', () => {


  beforeEach(() => {
    const spy = jasmine.createSpyObj('DemoService', ['getValue']);

    TestBed.configureTestingModule({
      providers: [
        MasterService,
        { provide: DemoService, useValue: spy}
      ]
    });
    masterService = TestBed.get(MasterService);
    demoServiceSpy = TestBed.get(DemoService);
  });

  it('#getValue should return stubbed value from spy', () => {

    const stubValue = 'stubbed value';
    demoServiceSpy.getValue.and.returnValue(stubValue);

    expect(masterService.getValue()).toBe(stubValue);

    expect(demoServiceSpy.getValue.calls.count()).toBe(1);

    expect(demoServiceSpy.getValue.calls.mostRecent().returnValue).toBe(stubValue);

  });



});

// describe('MasterService without Angular testing support', () => {
//   let masterService: MasterService;

//   it('#getValue should return real value from demo service', () => {
//     masterService = new MasterService(new DemoService());
//     expect(masterService.getValue()).toBe('real value');
//   });

  // it('#getValue should return fake value from fake service', () => {
  //   masterService = new MasterService(new FakeService());
  //   expect(masterService.getValue()).toBe('fake value');
  // });

  // it('#getValue should return value from fake object', () => {
  //   const fake = {
  //     getValue: () => 'fake value'
  //   };
  //   masterService = new MasterService(fake as DemoService);
  //   expect(masterService.getValue()).toBe('fake value');
  // });

  // it('#getValue should return stubbed value from spy', () => {
  //   const demoServiceSpy = jasmine.createSpyObj('DemoService', ['getValue']);

  //   const stubValue = 'stub value';

  //   demoServiceSpy.getValue.and.returnValue(stubValue);

  //   masterService = new MasterService(demoServiceSpy);

  //   expect(masterService.getValue()).toBe('stub value');

  //   expect(demoServiceSpy.getValue.calls.count())
  //     .toBe(1, 'spy method was called once');

  //   expect(demoServiceSpy.getValue.calls.mostRecent().returnValue)
  //     .toBe(stubValue);


  // });


  // it('#getValue should return stubbed value from a spy', () => {
  //   // create `getValue` spy on an object representing the ValueService
  //   const valueServiceSpy =
  //     jasmine.createSpyObj('DemoService', ['getValue']);
  //   // set the value to return when the `getValue` spy is called.
  //   const stubValue = 'stub value';
  //   valueServiceSpy.getValue.and.returnValue(stubValue);
 
  //   masterService = new MasterService(valueServiceSpy);
 
  //   expect(masterService.getValue())
  //     .toBe(stubValue, 'service returned stub value');
  //   expect(valueServiceSpy.getValue.calls.count())
  //     .toBe(1, 'spy method was called once');
  //   expect(valueServiceSpy.getValue.calls.mostRecent().returnValue)
  //     .toBe(stubValue);
  // });


// });
